var gulp = require('gulp');
var fs = require('fs');

var connect = require('gulp-connect');
var data = require('gulp-data');
var ejs = require('gulp-ejs');


var staticDir = './src/static';        // 静态资源目录
var tplDir = './src/templates';      // 页面
var distDir = './dist';          // 生成目录
var distHTML = distDir + "/html"
var distStaticDir = distDir + '/static';  // 发布静态资源目录

//开发环境任务
gulp.task('default', ['ejs', 'copy-static', 'ejs-watch', 'static-watch'], function () {
    gulp.watch(distDir + "/**/*.html").on('change', connect.reload);
});
//生产环境任务
gulp.task('runtimeTask', ['ejs', 'copy-static'], function () {
});

// 模版合并
gulp.task('ejs', function () {
    gulp.src(tplDir + '/**/*.html')
        .pipe(data(function (file) {
            return Object.assign(JSON.parse(fs.readFileSync(tplDir + '/global.json')), {
                local: {}
            })
        }))
        .pipe(ejs().on('error', function (err) {
            console.log(err);
        }))
        //发布文件夹
        .pipe(gulp.dest(distHTML))
});

gulp.task('ejs-watch', function () {
    gulp.watch(tplDir + '/**', ['ejs']);
});

gulp.task('static-watch', function () {
    gulp.watch(staticDir + '/**', ['copy-static']);
});

gulp.task('copy-static', function () {
    return gulp.src(staticDir + '/**')
        .pipe(gulp.dest(distStaticDir))
})